;;; Copyright (c) 2016 Zachary D. Meyer <zdm@opmbx.org>

;;; Permission to use, copy, modify, and distribute this software for any
;;; purpose with or without fee is hereby granted, provided that the above
;;; copyright notice and this permission notice appear in all copies.

;;; THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;;; Chapter 8 of 'Common Lisp - A Gentle Introduction to Symbolic
;;;; Computation' by David S. Touretzky

;;; Recursion

;;; 8.1 Use a trace to show how anyoddp would handle the list (3142
;;; 5798 6550 8914). Which cond clause is never true in this case?

;; The second cond clause is never true since there is no odd number
;; in the list.

;;; 8.2 Show how to write anyoddp using if instead of cond

(defun anyoddp (x)
  (if (null x) nil
      (if (oddp (first x)) t
	  (anyoddp (rest x)))))

;;; 8.4 Write the function called laugh that takes a number as input
;;; and returns a list of that many HAs.

(defun laugh (n)
  (cond ((<= n 0) nil)
	(t (cons 'ha (laugh (- n 1))))))

;;; 8.5 Write a function add-up to add up all the numbers in a list

(defun add-up (x)
  (cond ((null x) 0)
	(t (+ (first x) (add-up (rest x))))))

;;; 8.6 Write alloddp, a recursive function that returns t if all the
;;; members in a list are odd.

(defun alloddp (x)
  (cond ((null x) t)
	((oddp (first x)) (alloddp (rest x)))
	(t nil)))

;;; 8.7 Write a recursive version of member. Call it rec-member

(defun rec-member (item objects)
  (cond ((null objects) objects)
	((equal (first objects) item)
	 objects)
	(t (rec-member item (rest objects)))))

;;; 8.8 Write a recursive version of assoc, call it rec-assoc

(defun rec-assoc (item lists)
  (cond ((null lists) lists)
	((equal item (car (car lists)))
	 (car lists))
	(t (rec-assoc item (rest lists)))))

;; 8.9 Write a recursive version of nth, call it rec-nth

(defun rec-nth (n objects)
  (cond ((zerop n) objects)
	(t (rec-nth (- n 1) (rest objects)))))

;; 8.10 For x a nonnegative integer and y a positive integer, x+y
;; equals x+1+(y-1). If y is zero then x+y equals x. Use these
;; equations to build a recursive version of + called rec-plus out of
;; add1, sub1, cond, and zerop. You'll have to write add1 and sub1
;; too.

(defun add1 (num) (+ num 1))

(defun sub1 (num) (- num 1))

(defun rec-plus (x y)
  (cond ((zerop y) x)
	(t (rec-plus (add1 x) (sub1 y)))))

;; 8.11 Write a fibonacci function, fib(1) and fib(0) are both
;; considered to be 1.

(defun fib (n)
  (cond ((<= n 1) 1)
	(t (+ (fib (- n 1)) (fib (- n 2))))))

;; 8.12 Consider the following version of any-7-p, a recursive
;; function that searches a list for the number seven:

(defun any-7-p (x)
  (cond ((equal (first x) 7) t)
	(t (any-7-p (rest x)))))

;; Give a sample input for which this function will work
;; correctly. Give on for which the function will recurse infinitely.

(any-7-p '(2 3 4 7)) ; => t

(any-7-p nil) ; infinite

;; 8.14 Write the very shortest infinite recursion function you can.

(defun infinite ()
  (infinite))


;;; NOTES

;; Double-test recursion means we will have two end tests; if either
;; is true the corresponding value is returned and you don't recurse
;; any further. When both of those are false we end up at the last
;; cond clause which involves reducing the input somehow and calling
;; the function on that reduced input.

;; An example would be anyoddp. Our first end-test is checking whether
;; or not x is null. If it is, nil is returned and we don't recurse at
;; all. Our second end-test is to check if the first of x is odd, if
;; so t is returned and we don't recurse. If both fail we call anyoddp
;; on a reduced x and thus we repeat ourselves.

(defun anyoddp (x)
  (cond ((null x) nil)
	((oddp (first x)) t)
	(t (anyoddp (rest x)))))

;; 8.17 Use double-test tail recursion to write find-first-odd, a
;; function that returns the first odd number in a list, or nil if
;; there are none.

(defun find-first-odd (x)
  (cond ((null x) nil)
	((oddp (first x)) (first x))
	(t (find-first-odd (rest x)))))



;;; NOTES

;; Single-test tail recursion, as the name implies, only has a single
;; end-test. If that fails you call your function on the reduced input
;; This is a much simpler. This is mostly used in cases where you know
;; you're eventually going to find what you're looking for.

;; An example is find-first-atom which checks to see if x is an
;; atom. If not it calls itself on a reduced x, being (first ...), and
;; thus the cycle repeats. This function will always find an atom,
;; even if it's nil

(defun find-first-atom (x)
  (cond ((atom x) x)
	(t (find-first-atom (first x)))))


;; 8.18 Use single-test tail recursion to write last-element, a
;; function that returns the last element of a list. last-element
;; should recursively travel down the list until it reaches the last
;; cons cell (a cell whose cdr is an atom); then it should return the
;; car of this cell

(defun last-element (x)
  (cond ((equal (cdr x) nil) (first x))
	(t (last-element (rest x)))))

;; 8.19 Suppose we decided to convert anyoddp to single-test tail
;; recursion by simply eliminating the cond clause with the null
;; test. For which inputs would it still work correctly? What would
;; happen in those cases where it failed to work correctly?

(defun anyoddp (x)
  (cond ((oddp (first x)) t)
	(t (anyoddp (rest x)))))

;;; This function will work for any list that has an odd number, but
;;; for those that don't it will recurse infinitely.


;;; NOTES

;; Augmentation Recursion builds up the result bit by bit. Instead of
;; having an end-value be the final result, it adds that to the
;; previous calls.

;; For example, the count-slices function. The only test it has is to
;; check if loaf is null, this part is the "end journey". If that
;; fails, it goes to the last clause which adds 1 to the result of
;; calling itself onto a reduced loaf. This is an example of building
;; a result bit by bit. Instead of simply returning a value we build
;; up that value, this is augmentation.

(defun count-slices (loaf)
  (cond ((null loaf) 0)
	(t (+ 1 (count-slices (rest loaf))))))

;; Single-test Augmentation Recursion

(defun fact (n)
  (cond ((zerop n) 1) 
	(t (* n (fact (- n 1))))))      

;; 8.21 Write a recursive function add-nums that adds up the numbers
;; n, n-1, n-2, and so on, down to 0, and returns the result. For
;; example, (add-nums 5) should compute 5+4+3+2+1+0, which is 15

(defun add-nums (n)
  (cond ((zerop n) 0)
	(t (+ n (add-nums (- n 1))))))


;; 8.22 Write a recursive function all-equal that returns t if the
;; first element of a list is equal to the second, the second is equal
;; to the third, the third is equal to the fourth, and so
;; on. (all-equal '(I I I I)) should return t. (all-equal '(I I E I))
;; should return nil. all-equal should return t for lists with less
;; than two elements.

(defun all-equal (x)
  (cond ((null (rest x)) t)
	((not (equal (first x) (second x))) nil)
	(t (all-equal (rest x)))))

;; 8.24 Write count-down, a function that counts down from n using
;; list-consing recursion. (count-down 5) should produce the list (5 4
;; 3 2 1).

(defun count-down (n)
  (cond ((= 1 n) '(1))
	((zerop n) n)
	(t (cons n (count-down (- n 1))))))

;; 8.25 How could count-down be used to write an applicative version
;; of fact?

(defun fact-app (n)
  (reduce #'* (count-down n)))

;; 8.26 Suppose we wanted to modify count-down so that the list it
;; constructs ends in zero. For example, (count-down 5) would produce
;; (5 4 3 2 1 0).

(defun count-down (n)
  (cond ((zerop n) (list n))
	(t (cons n (count-down (- n 1))))))

;; 8.27 Write square-list, a recursive function that takes a list of
;; numbers as input and returns a list of their squares

(defun square-list (x)
  (cond ((null x) x)
	(t (cons (* (first x) (first x)) (square-list (rest x))))))

;; NOTES

;; Simultaneous Recursion on multiple variables is simply any form of
;; recursion that takes two inputs instead of 1. This can be done with
;; any recursion template.

(defun my-nth (n x)
  (cond ((zerop n) (first x))
	(t (my-nth (- n 1) (rest x)))))

;; 8.28 The expression (my-nth 5 '(a b c)) and (my-nth 1000000 '(a b
;; c)) both run off the end of the list and hence produce a nil
;; result. Yet the second expression takes quite a bit longer to
;; execute than the first. Modify my-nth so that the recursion stops
;; as soon as the function runs off the end of the list

(defun my-nth (n x)
  (cond ((null x) nil)
	((zerop n) (first x))
	(t (my-nth (- n 1) (rest x)))))

;; 8.29 Write my-member, a recursive version of member. This function
;; will take two inputs, but you will only want to reduce one of them
;; with each successive call. The other should remain unchanged.

(defun my-member (item objects)
  (cond ((null objects) nil)
	((equal item (first objects)) objects)
	(t (my-member item (rest objects)))))

;; 8.30 Write my-assoc, a recursive version of assoc.

(defun my-assoc (item objects)
  (cond ((null objects) nil)
	((equal item (first (first objects)))
	 (first objects))
	(t (my-assoc item (rest objects)))))

;; 8.31 Suppose we want to tell as quickly as possible whether one
;; list is shorter than another. If one list has five elements and the
;; other has a million, we don't want to have to go through all one
;; million cons cells before deciding that the second list is
;; longer. So we must not call length on the two lists. Write a
;; recursive function compare-length that takes two lists as input and
;; returns one of the following symbols: same-length, first-is-longer,
;; or second-is-longer.

(defun compare-length (x y)
  (cond ((and (null x) (null y)) 'same-length)
	((null y) 'first-is-longer)
	((null x) 'second-is-longer)
	(t (compare-length (rest x) (rest y)))))

;; 8.32 Write the function sum-numeric-elements, which adds up all the
;; numbers in a list and ignores the
;; non-numbers. (sum-numeric-elements '(3 bears 3 bowls and 1 girl)
;; should return seven

(defun sum-numeric-elements (x)
  (cond ((null x) 0)
	((numberp (first x))
	 (+ (first x)
	    (sum-numeric-elements (rest x))))
	(t (sum-numeric-elements (rest x)))))

;; 8.33 Write my-remove, a recursive version of the remove function.

(defun my-remove (item objects)
  (cond ((not (member item objects)) objects)
	((equal item (first objects))
	 (my-remove item (rest objects)))
	(t (cons (first objects) (my-remove item (rest objects))))))

;; 8.34 Write my-intersecion, a recursive version of the intersection
;; function.

(defun my-intersection (x y)
  (cond ((null x) nil)
	((member (first x) y)
	 (cons (first x) (my-intersection (rest x) y)))
	(t (my-intersection (rest x) y))))

;; 8.35 Write my-set-difference, a recursive version of the
;; set-difference function.

(defun my-set-difference (x y)
  (cond ((null x) nil)
	((not (member (first x) y))
	 (cons (first x) (my-set-difference (rest x) y)))
	(t (my-set-difference (rest x) y))))

;; 8.36 The function count-odd counts the number of odd elements in a
;; list of numbers; for example, (count-odd '(4 5 6 7 8)) should
;; return two. Show how to write count-odd using conditional
;; augmentation. Then write another using the regular augmenting
;; recursion template

(defun count-odd (x)
  (cond ((null x) 0)
	((oddp (first x))
	 (+ 1 (count-odd (rest x))))
	(t (count-odd (rest x)))))

(defun count-odd (x)
  (cond ((null x) 0)
	(t (+ (if (oddp (first x)) 1
		  0) (count-odd (rest x))))))

;; 8.39 Write a function count-atoms that returns the number of atoms
;; in a tree. (count-atoms '(a (b) c)) should return five, since in
;; addition to a, b, and c there are two nils in the tree.

(defun count-atoms (x)
  (cond ((atom x) 1)
	(t (+ (count-atoms (car x))
	      (count-atoms (cdr x))))))

;; 8.40 Write count-cons, a function that returns the number of cons
;; cells in a tree. (count-cons '(foo)) should return one. (count-cons
;; '(foo bar)) should return two. (count-cons '((foo))) should also
;; return two, since the list ((foo)) requires two cons
;; cells. (count-cons 'fred) should return zero.

(defun count-cons (tree)
  (cond ((atom tree) 0)
	(t (+ 1
	      (count-cons (car tree))
	      (count-cons (cdr tree))))))
;; 8.41 Write a function sum-tree that returns the sum of all numbers
;; appearing in a tree. Nonnumbers should be ignored. (sum-tree '((3
;; bears) (3 bows) (1 girl))) should return seven.

(defun sum-tree (x)
  (cond ((numberp x) x)
	((atom x) 0)
	(t (+ (sum-tree (car x))
	      (sum-tree (cdr x))))))

;; 8.43 Write flatten, a function that returns all the elements of an
;; arbritarily nested list in a single-level list. (flatten '((a b
;; (r)) a c (a d ((a (b)) r) a))) should return
;; (a b r a c a d a b r a)

(defun flatten (tree)
  (cond ((null tree) tree)
	((atom tree) (list tree))
	(t (append (flatten (car tree))
		   (flatten (cdr tree))))))

;; 8.44 Write a function tree-depth that returns the maximum depth of
;; a binary tree. (tree-depth '(a . b)) should return one. (tree-depth
;; '((a b c d))) should return five, and (tree-depth '((a . b) . (c
;; . d))) should return two.

(defun tree-depth (tree)
  (cond ((atom tree) 0)
	(t (+ 1 (tree-depth (car tree))
	      (tree-depth (cdr tree))))))

;; 8.45 Write a function paren-depth that returns the maximum depth of
;; nested parenteses in a list. (paren-depth '(a b c)) should return
;; one, whereas tree-depth would return three. (paren-depth '(a b ((c)
;; d) e)) should return three, since there is an element c that is
;; nested in three levels of parentheses

(defun paren-depth (alist)
  (cond ((atom alist) 0)
	(t (max (+ 1 (paren-depth (first alist)))
		(paren-depth (rest alist))))))

;; 8.47 Write make-loaf, a function that returns a loaf of size
;; n. (make-loaf 4) should return (x x x x). Use if instead of cond.

(defun make-loaf (n)
  (cond ((zerop n) nil)
	(t (append (make-loaf (- n 1)) (list 'x)))))

(defun make-loaf (n)
  (if (zerop n) nil
      (append (make-loaf (- n 1)) (list 'x))))

;; 8.48 Write a recursive function bury that burries an item under n
;; levels of parentheses. (bury 'fred 2) should return ((fred)), while
;; (bury 'fred 5) should return '(((((fred)))))

(defun bury (item n)
  (cond ((zerop n) item)
	(t (list (bury item (- n 1))))))

;; 8.49 Write pairings, a function that pairs the elements of two
;; lists. (pairings '(a b c) '(1 2 3)) should return ((a 1) (b 2) (c
;; 3)). You may assume that the two lists will be of equal length

(defun pairings (x y)
  (cond ((null x) nil)
	(t (cons (list (first x) (first y))
		 (pairings (rest x) (rest y))))))

;; 8.50 Write sublists, a function that returns the successive
;; sublists of a list. (siblists '(fee fie foe)) should return ((fee
;; fie foe) (fie foe) (foe)).

(defun sublists (x)
  (cond ((null x) nil)
	(t (cons x (sublists (rest x))))))

;; 8.51 The simplest way to write my-reverse, a recursive version of
;; reverse, is with a helping function plus a recursive function of
;; two inputs. Write this version of my-reverse.

(defun my-reverse (x)
  (cond ((null x) nil)
	(t (append (my-reverse (rest x)) (list (first x))))))

;; 8.52 Write my-union, a recursive version of union

(defun my-union (x y)
  (cond ((null x) y)
	((not (member (first x) y))
	 (cons (first x) (my-union (rest x) y)))
	(t (my-union (rest x) y))))

;; 8.53 Write largest-even, a recursive function that returns the
;; largest even number in a list of non-negative
;; integers. (largest-even '(5 2 4 3)) should return four. (largest
;; -even nil) should return zero

(defun get-even (x)
  (cond ((null x) nil)
	((evenp (first x)) (cons (first x)
				 (get-even (rest x))))
	(t (get-even (rest x)))))

(defun largest-even (x)
  (reduce #'max (get-even x)))

;; 8.54 Write a recursive function huge that raises a number to its
;; own power. (huge 2) should return 2^2, (huge 3) should return 3^3 =
;; 27, (huge 4) should return 4^4 = 256, and so on. Do not use reduce.

(defun help-huge (n counter)
  (cond ((= 1 n) counter)
	(t (* counter (help-huge (- n 1) counter)))))

(defun huge (n)
  (help-huge n n))

;;; KEYBOARD EXERCISE

;; In this exercise we will extract different sorts of information
;; from a genealogical database. The database gives information for
;; five generations of a family.

;; (name father mother)

(setf family
      '((colin nil nil)
	(deirdre nil nil)
	(arthur nil nil)
	(kate nil nil)
	(frank nil nil)
	(linda nil nil)
	(suzanne colin deirdre)
	(bruce arthur kate)
	(charles arthur kate)
	(david arthur kate)
	(ellen arthur kate)
	(george frank linda)
	(hillary frank linda)
	(andre nil nil)
	(tamara bruce suzanne)
	(vincent bruce suzanne)
	(wanda nil nil)
	(ivan george ellen)
	(julie george ellen)
	(marie george ellen)
	(nigel andre hillary)
	(frederick nil tamara)
	(zelda vincent wanda)
	(joshua ivan wanda)
	(quentin nil nil)
	(robert quentin julie)
	(olivia nigel marie)
	(peter nigel marie)
	(erica nil nil)
	(yvette robert zelda)
	(diane peter erica)))

;; The functions you write in this keyboard exercise need not be
;; recursive, except where indicated. For functions that return lists
;; of names, the exact order in which these names appear is
;; unimportant, but there should be no duplicates.

;;; 8.60

;; a. Write the functions father, mother, parents, and children that
;; returns a person's father, mother, a list of his or her known
;; parents, and a list of his or her children, respectively/ (father
;; 'suzanne) should return colin. (parents 'suzanne) should return
;; (colin deirdre). (parents 'frederick) should return (tamara), since
;; Frederick's father is unknown. (children 'arthur) should return the
;; set (bruce charles david ellen). If any of these functions is given
;; nil as input, it should return nil

(defun father (person)
  (second (assoc person family)))

(defun mother (person)
  (third (assoc person family)))

(defun parents (person)
  (remove-if #'(lambda (parent) (null parent))
	     (list (father person)
		   (mother person))))

(defun children (parent)
  (mapcar #'first
	  (remove-if-not #'(lambda (person)
			     (member parent (cdr person)))
			 family)))

;; b. Write siblings, a function that returns a list of a person's
;; siblings, including genetic half-siblings. (siblings 'bruce) should
;; return (charles david ellen). (siblings 'zelda) should return
;; (joshua).

(defun siblings (person)
  (remove person (reduce #'union (mapcar #'children (parents person)))))

;; c. Write mapunion, an applicative operator that takes a function
;; and a list as input, applies the function to every element of the
;; list, and computes the union of all the results. An example is
;; (mapunion #'rest '((1 a b c) (2 e c j) (3 f a b c d))), which
;; should return the set (a b c e j f d).

(defun mapunion (func alist)
  (reduce #'union (mapcar func alist)))

;; d. Write grandparents, a function that returns the set of a
;; person's grandparents. Use mapunion in your solution

(defun grandparents (person)
  (mapunion #'parents (parents person)))

;; e. Write cousins, a function that returns the set of a person's
;; genetically related first cousins, in other words, the children of
;; any of their parents' siblings.

(defun cousins (person)
  (mapunion #'children (mapunion #'siblings (parents person))))

;; f. Write the two-input recursive predicate descended-from that
;; returns a true value if the first person is descended from the
;; second. (descended-from 'tamara 'arthur) should return
;; t. (descended-from 'tamara 'linda) should return nil.

;; Hint: You are descended from someone if he is one of your parents,
;; or if either your father or mother is descended from him.

(defun descended-from (person person-origin)
  (cond ((null person) nil)
	((member person-origin (parents person)) t)
	(t (or (descended-from
		(father person) person-origin)
	       (descended-from
		(mother person) person-origin)))))

;; g. Write the recursive function ancestors that returns a person's
;; set of ancestors. (ancestors 'marie) should return the set (ellen
;; arthur kate george frank linda).

;; Hint: A person's ancestors are his parents plus his parents'
;; ancestors.

(defun ancestors (person)
  (cond ((null person) nil)
	(t (union (parents person)
		  (union (ancestors (father person))
			 (ancestors (mother person)))))))

(defun generation-gap (person-a person-b)
  (get-gap person-a person-b 0))

(defun get-gap (person-a person-b num)
  (cond ((null person-a) nil)
	((equal person-a person-b) num)
	(t (or (get-gap (father person-a) person-b (1+ num))
	       (get-gap (mother person-a) person-b (1+ num))))))

;; i. Use the functions you have written to answer the following
;; questions:

;;; 1. Is robert descended from deirdre?

;; No.

;;; 2. Who are Yvette's ancestors?

(WANDA VINCENT SUZANNE BRUCE ARTHUR KATE DEIRDRE COLIN LINDA FRANK GEORGE ELLEN
       QUENTIN JULIE ROBERT ZELDA)

;;; 3. What is the generation gap between Olivia and Frank?

3

;;; 4. Who are Peter's cousins?

(ROBERT JOSHUA)

;;; 5. Who are olivia's grandparents?

(HILLARY ANDRE GEORGE ELLEN)

;; 8.61 Write a tail-recursive version of count-up

(defun help-count (n result)
  (cond ((zerop n) result)
	(t (help-count (- n 1)
		       (cons n result)))))
(defun count-up (n)
  (help-count n nil))

;; 8.62 Write a tail-recursive version of factorial

(defun help-fact (n result)
  (cond ((or (= n 1) (<= n 0))
	 result)
	(t (help-fact (- n 1) (* n result)))))

(defun fact (n)
  (help-fact n 1))

;; 8.63 Write tail-recursive versions of union, intersection, and
;; set-difference. Your functions need not return results in the same
;; order as the built-in functions

(defun my-union (x y)
  (cond ((null x) y)
	((not (member (first x) y))
	 (cons (first x) (my-union (rest x) y)))
	(t (my-union (rest x) y))))

(defun my-intersection (x y)
  (cond ((null x) nil)
	((member (first x) y)
	 (cons (first x) (my-intersection (rest x) y)))
	(t (my-intersection (rest x) y))))

(defun help-set-difference (x y result)
  (cond ((null x) result)
	((not (member (first x) y))
	 (help-set-difference (rest x) y
			      (cons (first x) result)))
	(t (help-set-difference (rest x) y result))))

(defun  my-set-difference (x y)
  (help-set-difference x y nil))

;; 8.64 Write a tree-find-if operator that returns the first non-nill
;; atom of a tree that satisfies a predicate. (tree-find-if #'oddp
;; '((2 4) (5 6) 7)) should return 5

(defun tree-find-if (func set)
  (cond ((null set) set)
	((atom set) (if (funcall func set) set
			nil))
	(t (or (tree-find-if func (car set))
	       (tree-find-if func (cdr set))))))

;; 8.65 Use labels to write versions of tr-count-slices and tr-reverse

(defun tr-count-slices (x)
  (labels ((count-recursively (x result)
	     (if (null x) result
		 (count-recursively (rest x) (+ 1 result)))))
    (count-recursively x 0)))

(defun tr-reverse (set)
  (labels ((reverse-this (set result)
	     (if (null set) result
		 (reverse-this (rest set) (cons (first set) result)))))
    (reverse-this set nil)))

;; 8.66 Write arith-eval, a function that evaluates arithmetic expressions
;; (arith-eval '(2 + (3 * 4))) should return 14.

(defun arith-eval (expression)
  (cond ((or (null expression) (numberp expression)) expression)
	(t (funcall (second expression)
		    (arith-eval (first expression))
		    (arith-eval (third expression))))))

;; 8.67 Write a predicate legalp that returns t if its input is a
;; legal arithmetic expression. For example, (legalp 4) and (legalp
;; '((2 * 2) - 3)) should return t. (legalp nil and (legalp '(a b c
;; d)) should return nil.

(defun legalp (expression)
  (cond ((numberp expression) t)
	((atom expression) nil)
	(t (and (eq (length expression) 3)
		(legalp (first expression))
		(member (second expression) '(+ - * /))
		(legalp (third expression))))))
