;;;; Copyright (c) 2016 Zachary D. Meyer <zdm@opmbx.org>

;;;; Permission to use, copy, modify, and distribute this software for any
;;;; purpose with or without fee is hereby granted, provided that the above
;;;; copyright notice and this permission notice appear in all copies.

;;;; THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;;;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;;;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;;;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;;;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;;;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;;;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

;;; Output n spaces
(defun plot-string/output-space (n)
  (cond ((or (not (integerp n)) (minusp n))
	 (error "n must be a positive integer"))
	((zerop n) (format t ""))
	(t (format t " ")
	   (plot-string/output-space (1- n)))))

;;; Plot a string with y-value, the leftmost column is numbered zero.
(defun my-plot-points/plot-string (plotting-string y-val)
  (plot-string/output-space y-val)
  (format t "~A~%" plotting-string))

;;; Take a string and plot them according to the list of y-values
(defun plot-points (string y-values)
  (cond ((null y-values) (format t ""))
	(t (my-plot-points/plot-string string (first y-values))
	   (plot-points string (rest y-values)))))

;;; Generate a list of integers from m to n
(defun generate (m n)
  (cond ((= m n) (list n))
	((< m n) (cons m (generate (1+ m) n)))
	(t (cons m (generate (1- m) n)))))

;;; Graph a function over some values with a string.
(defun make-graph (func starting-value ending-value plotting-string)
  (plot-points plotting-string
	       (mapcar func (generate starting-value ending-value))))
