;;; Copyright (c) 2016 Zachary D. Meyer <zdm@opmbx.org>

;;; Permission to use, copy, modify, and distribute this software for any
;;; purpose with or without fee is hereby granted, provided that the above
;;; copyright notice and this permission notice appear in all copies.

;;; THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

;;;; Chapter 6 of 'Common Lisp - A Gentle Introduction to Symbolic
;;;; Computation' by David S. Touretzky

;;; 6.6 Use the last function to write a function called last-element that returns
;;; the last element of a list instead of the last cons cell. Write another version
;;; using reverse instead of last. Write another version using nth and length.

(defun last-element (x)
  (first (last x)))

(defun last-element-rv (x)
  (first (reverse x)))

(defun last-element-nl (x)
  (nth (- (length x) 1) x))

;;; 6.7 Use reverse to write a next-to-last function that returns the next-to-last
;;; element of a list. Write another version using nth.

(defun next-to-last (x)
  (second (reverse x)))

(defun next-to-last-nth (x)
  (nth (- (length x) 2) x))

;;; 6.8 Write a function my-butlast that returns a list with the last element
;;; removed. (my-butlast '(roses are red)) should return the list (roses are).
;;; (my-butlast '(g a g a)) should return (g a g).

(defun my-butlast (x)
  (reverse (rest (reverse x))))

;;; 6.9 What primitive function does the following reduce to?
(defun mystery (x) (first (last (reverse x))))

					; (first ...)

;;; 6.10 A palindrome is a sequence that reads the same forwards and backwards.
;;; The list (a b c d c b a) is a palindrome; (a b c a b c) is not. Write a
;;; function palindromep that returns t if its input is a palindrome.

(defun palindromep (x)
  (equal x (reverse x)))

;;; 6.11 Write a function make-palindrome that makes a palindrome out of a list,
;;; for example, given (you and me) as input it should return
;;; (you and me me and you).

(defun make-palindrome (x)
  (append x (reverse x)))

;;; 6.12 Does member have to copy its input to produce its result? Explain your
;;; reasoning.

;; No, it doesn't to because what member is going to return already exists.
;; It returns a pointer to a cons cell.

;; 6.13 What is the result of intersecting a set with nil?

;; nil is returned since that is the item appearing in both sets

;; 6.14 What is the result of intersecting a set with itself?

;; The original set is returned

;; 6.15 We can use member to write a predicate that returns a true value if a
;; sentence contains the word "the".

(defun contains-the-p (sent)
  (member 'the sent))

;; Suppose we instead want a predicate contains-article-p that returns a true value
;; if a sentence contains any article, such as "the", "a", or "an". Write a version
;; of this predicate using intersection. Write another version using member and or.
;; Could you solve this problem with and instead of or?

(defun contains-article-p (sent)
  (intersection '(the a an) sent))

(defun contains-article-por (sent)
  (or (member 'the sent)
      (member 'a sent)
      (member 'an sent)))

(defun member-article-pand (sent)
  (not (and (not (member 'the sent))
	    (not (member 'a sent))
	    (not (member 'an sent)))))

;; 6.16 What is the union of a set with nil?

;; The union of a set with nil is nil and the rest of the elements

;; 6.17 What is the union of a set with itself?

;; The set

;; 6.18 Write a function add-vowels that takes a set of letters as input and adds
;; the vowels (a e i o u) to the set. For example, calling add-vowels on the set
;; (x a e z) should produce the set (x a e z i o u), except that the exact order of
;; the elements in the result is unimportant

(defun add-vowels (x)
  (union x '(a e i o u)))

;; 6.19 What are the results of using nil as input to set-difference?

;; It depends, if nil is in the first arg and in the second, it will be removed
;; from the first. If it is in the first but not the second, it will stay

;; 6.20 Which of its two inputs does set-difference need to copy? Which input never
;; needs to be copied? Explain your reasoning.

;; The first input needs to be copied in order for set-difference to return a new
;; list with the removed elements. The second input never needs to be copied.

;; 6.21 If set x is a subset of set y, the subtracting y from x should leave the
;; empty set. Write my-subsetp, a version of the subsetp predicate that returns t
;; if its first input is a subset of its second input

(defun my-subsetp (x y)
  (not (set-difference x y)))

;; 6.22 Suppose the global variable a is bound to the list (soap water). What will
;; be the result of each of the following expressions?

(union a '(no soap radio)) ; => (no soap water radio)

(intersection a (reverse a)) ; => (soap water)

(set-difference a '(stop for water)) ; => (water)

(set-difference a a) ; => nil

(member 'soap a) ; => (soap water)

(member 'water a) ; => (water)

(member 'washcloth a) ; => nil

;; 6.23 The cardinality of a set is the number of elements it contains. What Lisp
;; primitive determines the cardinality of a set?

;; (length ...)

;; 6.24 Sets are said to be equal if they contain exactly the same elements. Order
;; does not matter in a set, so the sets (red blue green) and (green blue red) are
;; considered equal. However, the equal predicate does not consider them equal
;; because it treats them as lists, not as sets. Write a set predicate that returns
;; t if two things are equal as sets.

(defun set-equal (x y)
  (and (subsetp x y)
       (subsetp y x)))

;; 6.25 A set x is a proper subset of a set y if x is a subset of y but not equal
;; to y. Thus, (a c) is a proper subset of (c a b). (a b c) is a subset of (c a b)
;; but not a proper subset of it. Write the proper-subsetp predicate, which returns
;; t if its first input is a proper subset of its second input

(defun proper-subsetp (x y)
  (and (subsetp x y)
       (not (set-equal x y))))

;; 6.26 Write a program that compares the description of two objects
;; and tells how many features they have in common. The descriptions
;; will be represented as a list of features, with the symbol -vs-
;; seperating the first object from the second. Thus, when given a
;; list like (large red shiny cube -vs- small shiny red four-sided
;; pyramid) the program will respond with (2 common features).

;; a. Write a function right-side that returns all the features to the right of the -vs- symbol

(defun right-side (x)
  (cdr (member '-vs- x)))

;; b. Write a function left-side that returns all the features to the left of the -vs- symbol

(defun left-side (x)
  (cdr (member '-vs- (reverse x))))

;; c. Write a function count-common that returns the number of features the left and right
;; sides of the input have in common.

(defun count-common (x)
  (length (intersection
	   (left-side x) (right-side x))))

;; d. Write the main function, compare, that takes a list of features describing two objects,
;; with a -vs- between them, and reports the number of features they have in common. compare
;; should return a list of form (n common features)

(defun compare (x)
  (list (count-common x) 'common 'features))

;; 6.27 Should assoc be considered a predicate even though it never returns T?

;; It could be considered a predicate since it does return a non-nil value in the same sense
;; that member does.

;; 6.28 Set the global variable produce to this list

(setf produce
      '((apple    .  fruit)
	(celery   .  veggie)
	(banana   .  fruit)
	(lettuce  .  veggie)))

;; Now write down the results of the following expressions:

(assoc 'banana produce)  ; => (banana . fruit)

(rassoc 'fruit produce)  ; => (apple . fruit)

(assoc 'lettuce produce) ; => (lettuce . veggie)

(rassoc 'veggie produce) ; => (celery . veggie)

;; 6.29 What Lisp primitive returns the number of entries in a table?

;; length

;; 6.30 Make a table called books, of four books and their authors.

(setf books '((war-and-peace leo-tolstoy)
	      (1984 george-orwell)
	      (the-city-and-they-city china-mievile)
	      (neuromancer william-gibson)))

;; 6.31 Write a function who-wrote that takes the name of a book as input and
;; returns the book's author.

(defun who-wrote (title)
  (first (last (assoc title books))))

;; 6.32 Suppose we do (setf books (reverse books)), which reverses the order in
;; which the five books appear in the table. What will the who-wrote function do
;; now?

;; The who-wrote function will behave the same since reverse only reverse the
;; top-level list.

;; 6.34 Here is a table of states and some of their cities, stored in the global
;; variable atlas:

(setf atlas '((pennsylvania pittsburgh)
	      (new-jersey newark)
	      (pennsylvania johnstown)
	      (ohio columbus)
	      (new-jersey princeton)
	      (new-jersey trenton)))
;; Suppose we wanted to find all the cities a given state contains. assoc returns
;; only the first entry with a matching key, not all such entries, so for this
;; assoc cannot solve our problem. Redesign the table so that assoc can be used.

(setf atlas-corrected '((pennsylvania pittsburgh johnstown)
			(new-jersey newark princeton trento)
			(ohio columbus)))
;; 6.35 In this problem we will simulate the behavior of a very simple-minded
;; creature, Nerdus Americanis (also known as Computerus Hackerus). This creature
;; only has five states: Sleeping, Eating, Waiting-for-a-computer, Programming, and
;; Debugging. Its behavior is cyclic: After it sleeps it always eats, after it eats
;; it always waits for a computer, and so on, until after debugging it goes back to
;; sleep for a while.

;; a. What type of data structure would be useful for representing the connection
;; between a state and its successor? Write such a data structure for the
;; five-state cycle given above, and store it in a global variable called
;; nerd-states.

;; A table is a good data structure for this task

(setf nerd-states
      '((sleeping               .                  eating)
	(eating                 .  waiting-for-a-computer)
	(waiting-for-a-computer .             programming)
	(programming            .               debugging)
	(debugging              .                sleeping)))

;; b. Write a function nerdus that takes the name of a state as input and uses the
;; data structure you designed to determine the next state the creature will be in.

(defun nerdus (state)
  (cdr (assoc state nerd-states)))

;; c. What is the result of (nerdus 'playing-guitar)?

;; nil

;; d. When Nerdus Americanis ingests too many stimulants (caffeine overdose),
;; it stops sleeping. After finishing debugging, it immediately goes on to state
;; eating. Write a function sleepless-nerd that works just like nerdus except it
;; never sleeps.

(defun sleepless-nerd (state)
  (if (equal state 'debugging) 'eating
      (nerdus state)))

;; e. Exposing Nerdus Americanis to extreme amounts of chemical stimulants produces
;; pathological behavior. Instead of an orderly advance to its next state, the
;; creature advances two states. Write a function nerd-on-caffeine that exhibits
;; this unusual pathology.

(defun nerd-on-caffeine (state)
  (nerdus (nerdus state)))

;; If a nerd on caffeine is currently programming, how many states will it have to
;; go through before it is debugging?
()
;; 3

;; 6.36 Write a function to swap the first and last elements of any list.
;; (swap-first-last '(you can buy love)) should return (love cant buy you)

(defun swap-first-last (x)
  (let* ((old-reversed-list (reverse (rest x)))
	 (new-list (reverse (rest old-reversed-list))))
    (cons (first old-reversed-list)
	  (append new-list (list (first x))))))

;; 6.37 rotate-left and rotate-right are function that rotate the elements
;; of a list. (rotate-left '(a b c d e)) returns (b c d e a), whereas
;; rotate-right returns (e a b c d). Write these functions

(defun rotate-left (x)
  (append (rest x) (list (first x))))

(defun rotate-right (x)
  (append (last x) (reverse (rest (reverse x)))))

;; 6.40 Show how to tranform the list (a b c d) into a table so that the
;; assoc function using the table gives the same result as member using
;; the list

(setf my-lst
      '((a b c d)
	(b c d)
	(c d)
	(d)))

;; 6.42 Write a function called royal-we that changes every occurence of
;; the symbol I in a list to the symbol we.
(defun royal-we (x)
  (subst 'we 'i x))
