;;; Copyright (c) 2016 Zachary D. Meyer <zdm@opmbx.org>

;;; Permission to use, copy, modify, and distribute this software for any
;;; purpose with or without fee is hereby granted, provided that the above
;;; copyright notice and this permission notice appear in all copies.

;;; THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

;;;; Chapter 7 of 'Common Lisp - A Gentle Introduction to Symbolic
;;;; Computation' by David S. Touretzky

;; 7.1 Write an add1 function that adds one to its input. Then write
;; an expression to add one to each element of the list (1 3 5 7 9).

(defun add1 (x)
  (+ x 1))

(mapcar #'add1 '(1 3 5 7 9)) ; => (2 4 6 8 10)

;; 7.2 Let the global variable daily-planet contain the following
;; table:

(setq daily-planet
      '((olsen jimmy 123-76-4535 cub-reporter)
	(kent clark 089-52-6787 reporter)
	(lane lois 951-26-1438 reporter)
	(white perry 355-16-7439 editor)))

;; Each table entry consists of a last name, a first name, a social
;; security number, and a job title. Use mapcar on this table to
;; extract the list of social security numbers.

(mapcar #'third daily-planet)

;; => (|123-76-4535| |089-52-6787| |951-26-1438| |355-16-7439|)

;; 7.3 Write an expression to apply the zerop predicate to each
;; element of the list (2 0 3 4 0 -5 -6). The answer you get should be
;; a list of Ts and NILs.

(mapcar #'zerop '(2 0 3 4 0 -5 -6)) ; => (NIL T NIL NIL T NIL NIL)

;; 7.4 Suppose we want to solve a problem similar to the preceding
;; one, but instead of testing whether an element is zero, we want to
;; test whether it is greater than five. We can't use > directly for
;; this because > is a function of two inputs; mapcar will only give
;; it one input. Show how first writing a one-input function called
;; greater-than-five-p would help.

(defun greater-than-five-p (x)
  (> x 5))

(mapcar #'greater-than-five-p '(2 0 3 4 0 -5 -6))

;; => (NIL NIL NIL NIL NIL NIL NIL)

;; 7.5 Write a lambda expression to subtract seven from a number

#'(lambda (n) (- n 7))

;; 7.6 Write a lambda expression that returns t if the input is t or
;; nil, but nil for any other input

#'(lambda (x) (or (equal x t)
		  (equal x nil)))

;; 7.7 Write a function that takes a list such as (up down up up) and
;; "flips" each element, returning (down up down down). Your function
;; should include a lambda expression that knows how to flip an
;; individual element plus an applicative operator to do this to every
;; element of the list.

(defun flip (x)
  (mapcar #'(lambda (val)
	      (if (equal val 'up) 'down 'up)) x))

;; 7.8 Write a function that takes two inputs, x and k, and returns
;; the first number in the list x that is roughly equal to k. Let's
;; say that "roughly equal" means no less than k - 10 and no more than
;; k + 10.

(defun roughly-equal (x k)
  (find-if #'(lambda (num)
	       (and (> num (- k 10))
		    (< num (+ k 10)))) x))

;; 7.9 Write a function find-nested that returns the first element of
;; a list that is itself a non-NIL list.

(defun find-nested (nested-list)
  (find-if #'(lambda (item)
	       (not (equal item nil))) nested-list))

;; 7.10 In this exercise we will write a program to transpose a song
;; from on key to another. In order to manipulate notes more
;; efficiently, we will translate them into numbers. Here is the
;; correspondence between notes and numbers for a one-octave scale:

(setf note-table
      '((c 1)  (c-sharp 2)
	(d 3)  (d-sharp 4)
	(f 6)  (f-sharp 7)
	(g 8)  (g-sharp 9)
	(a 10) (a-sharp 11)
	(b 12)
	(e 5)))

;; a. Write a table to represent this information. Store it in a
;; global variable called note-table.

'(see above)

;; b. Write a function called numbers that takes a list of notes as
;; input and returns the corresponding list of numbers.  (numbers '(e
;; d c d e e e)) should return (5 3 1 3 5 5 5). This list represents
;; the first seven notes of "Marry Had a Little Lamb."

(defun numbers (notes)
  "Return a corresponding list of numbers"
  (mapcar #'(lambda (note)
	      (second (assoc note note-table)))
	  notes))

;; c. Write a function called notes that takes a list of numbers as
;; input and returns the corresponding list of notes.  (notes '(5 3 1
;; 3 5 5 5)) should return (e d c d e e e).  Hint: Since note-table is
;; keyed by note, assoc can't look up numbers in it; neither can
;; rassoc, since the elements are lists, not dotted pairs. Write your
;; own table-searching function to search note-table by number instead
;; of note


(defun notes (nums)
  "Return a corresponding list of notes"
  (let ((r-note-table (mapcar #'reverse note-table)))
    (mapcar #'(lambda (num)
		(second (assoc num r-note-table))) nums)))

;; d. Notice that notes and numbers are mutual inverses:

;; For x a list of notes:
;;     x    =     (notes (numbers x))

;; For x a list of numbers:
;;     x    =     (numbers (notes x))

;; What can be said about (notes (notes x)) and (numbers (numbers x))?

;; They both return a list of nils

;; e. To transpose a piece of music up by n half steps, we begine by
;; adding the value n to each note in the piece. Write a function
;; called raise that takes a number n and a list of numbers as input
;; and raises each number in the list by the value n. (raise '(5 3 1 3
;; 5 5 5)) should return (10 8 6 8 10 10 10), which is "Mary Had a
;; Little Lamb" transposed five half steps from the key of C to the
;; key of F.

(defun raise (steps nums)
  "Raise numbers by n half steps"
  (mapcar #'(lambda (num) (+ steps num)) nums))

;; f. Sometimes when we raise the value of a note, we may raise it
;; right into the next octave. For instance, if we raise the triad
;; c-e-g represented by the list (1 5 8) into the key of F by adding
;; five to each note, we get (6 10 13), or f-a-c. Here the c note,
;; represented by the number 13, is an octave above the regular c,
;; represented by 1. Write the function normalize that takes a list of
;; numbers as input and "normalizes" them to make them be between 1
;; and 12.  A number greater than 12 should have 12 subtracted from
;; it; a number less than 1 should have 12 added to it. (normalize '(6
;; 10 13)) should return (6 10 1).

(defun normalize (nums)
  "Normalize numbers by making them be between 1 and 12."
  (mapcar #'(lambda (num)
	      (cond ((> num 12) (- num 12))
		    ((< num 1) (+ num 12))
		    (t num)))
	  nums))

;; g. Write a function transpose that takes a number n and a song as
;; input, and returns the song tranposed by n half steps. (tranpose 5
;; '(e d c d e e e)) should return (a g f g a a a). Your solution
;; should assume the availability of the numbers notes, raise, and
;; normalize functions.

(defun transpose (steps song)
  "Return song tranposed by n half steps"
  (notes (normalize (raise steps (numbers song)))))

;; 7.11 Write a function to pick out those numbers in a list that are
;; greater than one and less than five

(defun myfunc (nums)
  (remove-if-not #'(lambda (num)
		     (and (> num 1) (< num 5))) nums))

;; 7.12 Write a function that counts how many time the word 'the'
;; appears in a sentence

(defun how-many-the (sentence)
  (length (remove-if-not #'(lambda (word)))
	  (equal 'the word) sentence))

;; 7.13 Write a function that picks from a list of lists those of
;; exactly length two

(defun pick-2 (lists)
  (remove-if-not #'(lambda (list)
		     (equal 2 (length list))) lists))

;; 7.15 In this keyboard exercise we will manipulate playing cards
;; with applicative operators A card will be represented by a list of
;; form (rank suit), for example, (ace spades) or (2 clubs). A hand
;; will be represeneted by a list of cards.

;; a. Write the functions rank and suit that return that rank and suit
;; of a card, respectively. (rank '(2 clubs)) should return 2, and
;; (suit '(2 clubs)) should return clubs.

(defun rank (card)
  (first card))

(defun suit (card)
  (second card))

;; b. Set the global variable my-hand to the following hand of cards:

(defvar my-hand
  '((3 hearts)
    (5 clubs)
    (2 diamonds)
    (4 diamonds)
    (ace spades)))

;; Now write a function count-suit that takes two-inputs, a suit and a
;; hand of cards, and returns the number of cards belonging to that
;; suit. (count-suit 'diamonds my-hand) should return 2

(defun count-suit (suit cards)
  (length (remove-if-not #'(lambda (card)
			     (equal suit (suit card))) cards)))

;; c. Set the global variable colors to the following table:

(defvar colors '((clubs black)
		 (diamonds red)
		 (hearts red)
		 (spades black)))

;; Now write a function color-of that uses the table colors to
;; retrieve the color of a card.  (color-of '(2 clubs)) should return
;; black. (color-of '(6 hearts)) should return red.

(defun color-of (card)
  (second (assoc (suit card) colors)))

;; d. Write a function first-red that returns the first card of a hand
;; that is of a red suit or NIL if none are

(defun first-red (cards)
  (find-if #'(lambda (card)
	       (equal (color-of card) 'red)) cards))

;; e. Write a function black-cards that returns a list of all the
;; black cards in a hand.

(defun black-cards (cards)
  (remove-if-not #'(lambda (card)
		     (equal (color-of card) 'black)) cards))

;; f. Write a function what-ranks that takes two inputs, a suit and a
;; hand, and returns the ranks of all cards belonging to that
;; suite. (what-ranks 'diamonds my-hand) should return the list (2
;; 4). (what-ranks 'spades my-hand should return the list (ace). Hint:
;; First extract all the cards of the specified suit, then use another
;; operator to get the ranks of those cards

(defun ranks (suit hand)
  (mapcar #'rank (remove-if-not #'(lambda (card)
				    (equal (suit card) suit)) hand)))

;; g. Set the global variable all-ranks to the list

(defvar all-ranks '(2 3 4 5 6 7 8 9 10 jack queen king ace))

;; Then write a predicate higher-rank-p that takes two cards as input
;; and returns true if the first card has a higher rank than the
;; second. Hint: look at the beforep predicate on page 171 of Chapter
;; 6.

(defun higher-rank-p (card1 card2)
  (member card2 (member card1 all-ranks)))

;; h. Write a function high-card that returns the highest ranked card
;; in a hand.  Hint: One way to solve this is to use find-if to search
;; a list of ranks (ordered from high to low) to find the highest rank
;; that appears in the hand.  Then use assoc on the hand to pick the
;; card with that rank

(defun high-card (hand)
  (assoc (find-if #'(lambda (rank)
		      (assoc rank hand)) (reverse all-ranks)) hand))

;; 7.16 Suppose we had a list of sets ((a b c) (c d a) (f b d) (g))
;; that we wanted to collapse into one big set. If we use append for
;; our reducing function, the result won't be a true set, because some
;; elements will appear more than once.  What reducing function should
;; be used instead?

(reduce #'union '((a b c) (c d a) (f b d) (g))) ; => (d b f c a g)

;; 7.17 Write a function that, given a lists of lists, returns the
;; total length of all the lists. This problem can be solved two
;; different ways

(defun total-length (x)
  (length (reduce #'append x)))

(defun total-length-other (x)
  (reduce #'+ (mapcar #'length x)))

;; 7.19 Write a function all-odd that returns t if every element of a
;; list of numbers is odd.

(defun all-odd (x)
  (every #'oddp x))

;; 7.20 Write a function none-odd that returns t if every element of a list of
;; numbers is not odd.

(defun none-odd (x)
  (every #'(lambda (num)
	     (not (oddp num))) x))

;; 7.21 Write a function not-all-odd that returns t if not every element of a list
;; of numbers is odd.

(defun not-all-odd (x)
  (not (every #'oddp x)))

;; 7.22 Write a function not-none-odd that returns t if it is not the case that a
;; list of numbers contains no odd elements.

(defun not-none-odd (x)
  (not (every #'evenp x)))

;; 7.24 What is an applicative operator?

;; An operator that takes a function as an argument/input.

;; 7.25 Why are lambda expressions useful? Is it possible to do without them?

;; Lambda expressions are useful in scenarios where an applicative operator only
;; takes functions that have 1 argument. You can use a lambda to do more complex
;; operations. It's creating anonymous functions inside a form. You can go without
;; lambda expressions by creating a function with a single argument beforehand.


;;; In this keyboard exercise we will develop a system for representing
;;; knowledge about "blocks world". Assertions about the objects in a scene are
;;; represented as triples of form (block attribute value). Here are some
;;; assertions about block B2's attributes:

;; (b2 shape brick)
;; (b2 color red)
;; (b2 size small)
;; (b2 supports b1)
;; (b2 left-of b3)

;;; A collection (in other words, a list) of assertions is called a database.

;;; Given a database describing the blocks in the figure, we can write functions
;;; to answer questions such as, "What color is block b2?" or "What blocks
;;; support block b1?" To answer these questions we will use a function called a
;;; pattern matcher to search the database for us. For example, to find out the
;;; color of block b2, we use the pattern (b2 color ?).

;; > (fetch '(b2 color ?)) => ((b2 color red))

;;; To find which blocks support b1, we use the pattern (? supports b1):

;; (fetch '(? supports b1)) => ((b2 supports b1) (b3 supports b1))

;;; Fetch returns those assertions from the database that match a given
;;; pattern. It should be apparent from the preceding examples that a pattern is
;;; a triple, like an assertion, with some of its elements replaced by question
;;; marks.

;;; A question mark in a pattern means any value can match in that
;;; position. Thus, the pattern (b2 color ?) can match assertions like (b2 color
;;; red), (b2 color green), (b2 color blue), and so on. It cannot match the
;;; assertion (b1 color red) because the first element of the pattern is the
;;; symbol b2, whereas the first element of the assertion is b1.

(defvar database '((b1 shape brick)
		   (b1 color green)
		   (b1 size small)
		   (b1 supported-by b2)
		   (b1 supported-by b3)
		   (b2 shape brick)
		   (b2 color red)
		   (b2 size small)
		   (b2 supports b1)
		   (b2 left-of b3)
		   (b3 shape brick)
		   (b3 color red)
		   (b3 supports b1)
		   (b3 right-of b2)
		   (b4 shape pyramid)
		   (b4 color blue)
		   (b4 size large)
		   (b4 supported-by b5)
		   (b5 shape cube)
		   (b5 color green)
		   (b5 size large)
		   (b5 supports b4)
		   (b6 shape brick)
		   (b6 color purple)
		   (b6 size large)
		   (b2 same-as b2)))

;;; a. Write a function match-element that takes two symbols as inputs. If the
;;; two are equal, or if the second is a question mark, match element should
;;; return t. Otherwise it should return nil.

(defun match-element (x y)
  "Return true if x and y are equal or if y is equal to '?"
  (or (equal x y)
      (equal y '?)))

;;; b. Write a function match-triple that takes an assertion and a pattern as
;;; input, and returns t if the assertion matches the pattern. Both inputs will
;;; be three-element lists.

(defun match-triple (assertion pattern)
  "Return true if assertion matches pattern"
  (every #'match-element assertion pattern))

;;; c. Write a function fetch that takes a pattern as input and returns all
;;; assertions in the database that match the pattern.

(defun fetch (pattern)
  "Return all assertions in the database that match the pattern"
  (remove-if-not #'(lambda (assertion)
		     (match-triple assertion pattern)) database))

;;; e. (d skipped, not part of writing functions)
;;; Write a function that takes a block name as input and returns a pattern
;;; asking the color of the block.

(defun get-color-pattern (block-name)
  "Return the pattern to get the color for the block"
  (append (list block-name) '(color ?)))

;;; f. Write a function supporters that takes one input, a block, and returns a
;;; list of the blocks that support it.

(defun supporters (block-name)
  "Return a list of blocks that support block-name"
  (mapcar #'first (fetch (append '(? supports)
				 (list block-name)))))

;;; g. Write a predicate supp-cube that takes a block as input and returns true
;;; if that block is supported by a cube.

(defun supp-cube (block-name)
  "Return a true value if block-name is supported by a cube"
  (fetch (append (supporters block-name) '(shape cube))))

;;; h. Write a function desc1 that takes a block as input and returns all
;;; assertions dealing with that block.

(defun desc1 (block-name)
  "Return all assertions dealing with block-name"
  (fetch (list block-name '? '?)))

;;; i. Write a function desc2 of one input that calls desc1 and strips the block
;;; name off each element of the result.

(defun desc2 (x)
  "Strip the block name off each element"
  (mapcar #'(lambda (element) (rest element))
	  (desc1 x)))

;;; j. Write the description funtion. It should take one input, call desc2, and
;;; merge the resulting list of lists into a single list.

(defun description (x)
  "Return a description of a block"
  (reduce #'append (desc2 x)))
